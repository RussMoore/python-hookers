from quit_now import quit_now
from fish_action import fish_start


def print_commands(hero):
    #print basic commands: 'look in bag', 'look in wallet', 'look in bucket', 'look at everything'
    print(f"'q': quit\n'b': look in bag\n'w': look in wallet\n'f': fish at the {hero.location}\n'c': print current commands")
    #print location dependent commands (i.e., if at dock, 's' is for store)

def prompt_user(command, hero, store):
    reprompt = False # if true, will reprint commands
    if command == 'q':
            quit_now()
            #quit()
    elif command == 'b':
        hero.look_at_inv('bag')
    elif command == 'w':
        hero.look_at_inv('wallet')
    elif command == 'c':
         print_commands(hero)
    elif hero.location == 'dock':
        if command == 's':
            #go to the store
            store.introduce_store()
            #store.prompt_user()

            pass
        elif command == 'c':
            #captain your ship
            pass
        elif command == 'f':
            #fish from the dock
            reprompt = fish_start('trout') # needs location and fish object

            pass

    if reprompt:
         print_commands(hero)
         #command = input()
         #prompt_user(command, hero, store)
    return hero

#reformat the code to be nested if statement that first asks user loaction (dock, )
