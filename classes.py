class Character():
    def __init__(self, money, gear_list, strength, name='hero', location='dock', luck=10, selected_gear=[]):
        self.name = name
        self.money = money
        self.gear_list = gear_list
        self.strength = strength
        self.location = location
        self.luck = luck # out of 100?
        self.selected_gear = selected_gear



    def look_at_inv(self, container='all'):

        if container == 'all':
            print("gear in gear list: ")
            print(self.gear_list)
            print('--------')
            print("cash: ", self.money)
            print("strenth: ", self.strength)
        elif container == 'bag':
            print("gear in gear list: ")
            print(self.gear_list)
            print('--------')
        elif container == 'wallet':
            print(f"${self.money}")
        else:
            print("hmm... what do you want?")

    def go_to_location(self, location):
        self.location = location

class Store():
    def __init__(self):
        self.products = {
            'basic worm bait': 2,
            'broken fishing rod': 4,
            'golden fishing rod': 100,
            'fishing line': 2,
            'tackle box': 6,
        }
        self.been_visited = False

    def introduce_store(self):
        if not self.been_visited:
            print("At the fishing shop in Python Hookers, you will find a wide range of items to enhance your fishing experience. Here are some of the items you can expect to find:")
            self.been_visited = True
            print(self.products)

class Weather():
    def __init__(self, temperature, humidity, wind_direction, wind_speed, precipitation):
        self.temperature = temperature
        self.humidity = humidity
        self.wind_direction = wind_direction
        self.wind_speed = wind_speed
        self.precipitation = precipitation
