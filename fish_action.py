#import fish class
import os
from datetime import datetime, timedelta
from time import sleep

def fish_start(fish, player=None, chapter=None):
    pass
    # chapter will return object of where in stroy the player is
    #wait a few seconds
    # match up player gear with fish gear being used
    #player will need a "gear being used" attribute
    #fish will have weight, rarity


    #checks location
    #checks where in story the player is

    #run while loop until fish/dud is caught
    fish_caught = False
    fish_lost = False
    over_tension = False
    sensitivity = 60000 #(lower is more senitive)
    game_duration = 5

    total_time_elapsed = timedelta(microseconds=0)
    sleep(1)
    print('...')
    sleep(1)
    print('... just waitin for a bite...')
    sleep(1)

    max_tension = 20 #higher is easier
    min_tension = 2 #lower is easier
    tension = (20-2)//2
    print("woah now!!")
    sleep(1)
    while not fish_caught and not fish_lost:
        os.system('cls' if os.name == 'nt' else 'clear')
        sleep(.0004)
        #print(" "*(max_tension-2), 'V')#
        #print(' '*(min_tension-1), 'V')#
        print(f"{' '*min_tension}min{' '*(max_tension-min_tension-4)}max")
        print(f"{' '*min_tension}V{' '*(max_tension-min_tension-1)}V")
        start = datetime.now() #time before printout
        response = input("|"*tension)
        end = datetime.now() #time after response
        delta_time_current = end-start
        #print(delta_time_current)
        if delta_time_current < timedelta(microseconds=sensitivity*1.5):
            #print('tested: ', timedelta(microseconds=20000))
            tension +=2
        elif delta_time_current < timedelta(microseconds=sensitivity*2):
            tension += 1
        elif delta_time_current > timedelta(microseconds=sensitivity*3):
            tension -=3
        elif delta_time_current > timedelta(microseconds=sensitivity*2.5):
            tension -= 2
        elif delta_time_current > timedelta(microseconds=sensitivity*2):
            tension -= 1
        else:
            tension = 1
        if tension < min_tension:
            fish_lost = True
        if tension > max_tension:
            fish_lost = True
            over_tension = True
        #if delta_time >
        #delta
        if total_time_elapsed > timedelta(seconds=game_duration):
            fish_caught = True
        total_time_elapsed += delta_time_current
        delta_time_last = delta_time_current
    if fish_lost:
        print(f'She got away!!')
    elif fish_caught:
        print(f'you caught a nice {fish}!')
    if over_tension:
        print(f'Damn, you put a lil too much elbow grease in that joint.')
    sleep(.5)
    return True # used to see if prompt user needs to run again
